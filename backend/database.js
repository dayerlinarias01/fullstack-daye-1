const mongoose = require('mongoose');

mongoose.connect(process.env.MONGODB_URI,{
    useBigInt64: true
})
    .then(db => console.log('DB is connected'))
    .catch(err => console.error(err));